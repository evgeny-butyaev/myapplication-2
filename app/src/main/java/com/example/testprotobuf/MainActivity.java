package com.example.testprotobuf;

import androidx.appcompat.app.AppCompatActivity;

import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.navitag.TrackerPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long _timeStamp = (new Date()).getTime();

        long _timeStamp_device = (new Date()).getTime();

        byte[]  deviceProfilePackage = Packer.createDeviceProfilePackage(
                1000,
                "6RLWW5S8C6V9",
                "1.0.0.0",
                "ANDROID, API28",// + android.os.Build.VERSION.SDK_INT,
                true //m_service.isNavitelInstalled() // XXX
        );

        // Log.d("testproto_debug", "deviceProfilePackage timestamp: " +String.valueOf(_timeStamp_device));
        Log.d("testproto_debug", "deviceProfilePackage \n" + Packer.byteArrayToHex(deviceProfilePackage));


        LocationDebug location = new LocationDebug(LocationManager.NETWORK_PROVIDER);
        long curMils = System.currentTimeMillis();
        location.setTime(curMils);
        location.setLongitude(11.22);
        location.setLatitude(33.44);
        location.setSpeed((float) 0);
        location.setBearing(20);
        location.setAltitude(150);
        location.setAccuracy(30);
        //location.setProvider("gps");
        Bundle extraBundle = new Bundle();
        extraBundle.putInt("satellites", 8);
        location.setExtras(extraBundle);

//                    Location oldLocation = new Location(LocationManager.NETWORK_PROVIDER);
//                    curMils = System.currentTimeMillis()-2;
//                    oldLocation.setTime(curMils /*1621677859*/);
//                    oldLocation.setLongitude(12.222);
//                    oldLocation.setLatitude(67.777);
//                    oldLocation.setSpeed((float) 0.55);
//                    oldLocation.setBearing(20);
//                    oldLocation.setAltitude(127);
//                    oldLocation.setAccuracy(56);
//                    //location.setProvider("gps");
//                    extraBundle = new Bundle();
//                    extraBundle.putInt("satellites", 8);
//                    oldLocation.setExtras(extraBundle);

        TrackerPoint point = new TrackerPoint(location, null /*oldLocation*/, this);

        ArrayList<TrackerPoint> points = new ArrayList<TrackerPoint>();
        points.add(point);

        long _timeStamp_track = (new Date()).getTime();
        byte[] dataTrackPack = Packer.createTrackDataPackage(
                1001,
                "6RLWW5S8C6V9",
                points
        );

        Log.d("testproto_debug", "dataTrackPack timestamp: " +String.valueOf(_timeStamp_track));
        Log.d("testproto_debug", "dataTrackPack location ms: " +String.valueOf(curMils));
        Log.d("testproto_debug", "dataTrackPack location: " + location.toString());
        Log.d("testproto_debug", "dataTrackPack \n" + Packer.byteArrayToHex(dataTrackPack));



        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //Replace below IP with the IP of that device in which server socket open.
                    //If you change port then change the port number in the server side code also.
                    Socket s = new Socket("193.232.47.4", 30159);
                    OutputStream out = s.getOutputStream();
                    PrintWriter output = new PrintWriter(out);
                    BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));

                    out.write(deviceProfilePackage);
                    output.flush();

                    final String deviceProfileReply = input.readLine();
                    final boolean post = handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("testproto_debug", "Reply from server \n" + deviceProfileReply);
                            Log.d("testproto_debug", "Reply as byte arr \n" + Packer.byteArrayToHex(deviceProfileReply.getBytes(), true));
                        }
                    });

//                    try {
//                        Thread.sleep(2000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                    out.write(dataTrackPack);
                    output.flush();

                    final String dataTrackReply = input.readLine();
                    final boolean post2 = handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("testproto_debug", "Reply from server \n" + dataTrackReply);
                            Log.d("testproto_debug", "Reply as byte arr \n" + Packer.byteArrayToHex(dataTrackReply.getBytes(), true));
                        }
                    });

                    output.close();
                    out.close();
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }
}