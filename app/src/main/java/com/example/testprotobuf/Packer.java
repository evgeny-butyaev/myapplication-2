package com.example.testprotobuf;

import android.location.LocationManager;
import android.util.Log;

import com.navitag.TrackerPoint;
import com.navitag.TrackerProtocol;
import com.navitag.utils.ChecksumGenerator;
import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

//public class Packer {
//}

//package com.navitag;


/**
 * Created by eugene on 5/19/17.
 */
public class Packer {
    static short version = 1;

    //static private ServiceSettings settings() { return ServiceSettings.instance(); }

    static private byte[] serializedProtobufToPackage(byte[] _protobuf, boolean is_ping) {
        return ByteBuffer
                .allocate((Integer.SIZE + 2 * Short.SIZE) / Byte.SIZE + _protobuf.length)
                .order(ByteOrder.BIG_ENDIAN)
                .putShort(version) //s1
                .putInt((int) _protobuf.length) // i1
                .putShort(new ChecksumGenerator().getCrc16(_protobuf)) //s2
                .put(_protobuf).array();
    }

    public static String byteArrayToHex(byte[] a) {
        return  byteArrayToHex(a, false);
    }

    public static String byteArrayToHex(byte[] a, Boolean printASCIIChars ) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a) {

            if (!printASCIIChars) {

                sb.append("\\x");
                sb.append(String.format("%02x", b).toUpperCase());
            } else {

                if (b<32) {
                    sb.append("\\x");
                    sb.append(String.format("%02x", b).toUpperCase());
                } else {
                    sb.append((char)b);
                }
            }

        }
        return sb.toString();
    }

    public static byte[] createDeviceProfilePackage(
            long _timeStamp,
            String _deviceId,
            String _appVersion,
            String _platformVersion,
            boolean _supportsAssistanceRequests
    ) {

        TrackerProtocol.DeviceProfile.Builder deviceProfileBuilder =
                TrackerProtocol.DeviceProfile.newBuilder()
                        .setSupportsAssistanceRequests(_supportsAssistanceRequests)
                        .setAppVersion(_appVersion)
                        .setPlatform(_platformVersion);

        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
                TrackerProtocol.NavitagClientMessage.newBuilder()
                        .setMessageId(_timeStamp)
                        .setDeviceid(_deviceId)
                        .setDeviceProfile(deviceProfileBuilder);

        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();

        Log.d("testproto_debug", byteArrayToHex(protobufSerialized));

        return serializedProtobufToPackage(protobufSerialized, false);
    }


    static public TrackerProtocol.TrackData.TrackPointInfo.Builder getTrackPointInfoBuilder(TrackerPoint _tp) {
        TrackerProtocol.TrackData.TrackPointInfo.NavInfo.Builder navInfoBuilder =
                TrackerProtocol.TrackData.TrackPointInfo.NavInfo.newBuilder()
                        .setCapturedTimestamp(_tp.timeSecondsFromEpoh)
                        .setLonDeg(_tp.longitude)
                        .setLatDeg(_tp.latitude)
                        .setAltitudeM(_tp.altitudeMeters)
                        .setSpeedKmh(_tp.speedKmPerHour)
                        .setAccuracyM(_tp.accuracyMeters)
                        .setDirectionDeg(_tp.bearing)
                        .setSatelliteCount(_tp.maxSatellites)
                        .setLocationProvider(
                                _tp.provider.equals(LocationManager.GPS_PROVIDER) ?
                                        TrackerProtocol.LocationProvider.GPS :
                                        TrackerProtocol.LocationProvider.Network
                        );

        TrackerProtocol.TrackData.TrackPointInfo.DeviceInfo.Builder deviceInfoBuilder =
                TrackerProtocol.TrackData.TrackPointInfo.DeviceInfo.newBuilder()
                        .setBatteryChargePcnt(_tp.batteryPercent)
                        .setBatteryPowerMv(_tp.batteryMv)
                        .setTemperatureC(_tp.batteryTemperatureDegreesCelsius);

        TrackerProtocol.TrackData.TrackPointInfo.Builder trackPointInfoBuilder =
                TrackerProtocol.TrackData.TrackPointInfo.newBuilder()
                        .setNavInfo(navInfoBuilder)
                        .setDeviceInfo(deviceInfoBuilder);

        return trackPointInfoBuilder;
    }
//
//    static public byte[] createAckDataPackage(
//            long _timeStamp,
//            String _deviceId,
//            long _ackedMessageId) {
//        TrackerProtocol.Ack.Builder ackBuilder =
//                TrackerProtocol.Ack.newBuilder().setMessageId(_ackedMessageId);
//
//        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
//                TrackerProtocol.NavitagClientMessage.newBuilder()
//                        .setMessageId(_timeStamp)
//                        .setDeviceid(_deviceId)
//                        .setAck(ackBuilder);
//
//        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();
//
//        return serializedProtobufToPackage(protobufSerialized, false);
//    }
//
    static public byte[] createTrackDataPackage(
            long _timeStamp,
            String _deviceId,
            ArrayList<TrackerPoint> _points
    ) {
        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
                TrackerProtocol.NavitagClientMessage.newBuilder()
                        .setMessageId(_timeStamp)
                        .setDeviceid(_deviceId);

//        if (settings().getReportStatuses() && !settings().getStatus().isEmpty())
//            navitagMessageBuilder.setClientStatus(settings().getStatus());

        for (TrackerPoint tp : _points) {
            TrackerProtocol.TrackData.TrackPointInfo.Builder tpiBuilder = getTrackPointInfoBuilder(tp);

            navitagMessageBuilder.addTrackPointInfo(tpiBuilder);
        }

        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();

        return serializedProtobufToPackage(protobufSerialized, false);
    }
//
//    public static byte[] createEmptyNavitelClientMessage() {
//        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
//                TrackerProtocol.NavitagClientMessage.newBuilder();
//        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();
//        return serializedProtobufToPackage(protobufSerialized, false);
//    }
//
//    public static byte[] createAssistanceResponsePackage(
//            long _timeStamp,
//            String _deviceId,
//            long _requestId,
//            TrackerProtocol.RequestStatus _status
//    ) {
//        TrackerProtocol.AssistanceRequestStatus.Builder assistanceStatusBuilder =
//                TrackerProtocol.AssistanceRequestStatus.newBuilder()
//                        .setId(_requestId)
//                        .setStatus(_status);
//
//        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
//                TrackerProtocol.NavitagClientMessage.newBuilder()
//                        .setMessageId(_timeStamp)
//                        .setDeviceid(_deviceId)
//                        .setAssistanceStatus(assistanceStatusBuilder);
//
//        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();
//
//        return serializedProtobufToPackage(protobufSerialized, false);
//    }
//
//    public static byte[] createChatMessagePackage(
//            long _timeStamp,
//            String _deviceId,
//            ChatMessage _msg
//    ) {
//        TrackerProtocol.ChatMessage.Builder ChatMessageBuilder =
//                TrackerProtocol.ChatMessage.newBuilder()
//                        .setId(_msg.id)
//                        .setTime(_msg.time_sec)
//                        .setTheme(_msg.theme)
//                        .setBody(_msg.body)
//                        .setRequestIdReserved(_msg.request_id);
//
//        if (!_msg.photo_path.equals(""))
//            ChatMessageBuilder.setPhotoReserved(ByteString.copyFrom(_msg.getPhoto()));
//
//        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
//                TrackerProtocol.NavitagClientMessage.newBuilder()
//                        .setMessageId(_timeStamp)
//                        .setDeviceid(_deviceId)
//                        .setChatMessage(ChatMessageBuilder);
//
//        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();
//
//        return serializedProtobufToPackage(protobufSerialized, false);
//    }
//
//    public static byte[] createChatInfoPackage(
//            long _timeStamp,
//            String _deviceId,
//            long[] _chatMsgIds
//    ) {
//        TrackerProtocol.ChatInfo.Builder chatInfoBuilder = TrackerProtocol.ChatInfo.newBuilder();
//
//        for (long id : _chatMsgIds)
//            chatInfoBuilder.addOnreadNotifications(id);
//
//        TrackerProtocol.NavitagClientMessage.Builder navitagMessageBuilder =
//                TrackerProtocol.NavitagClientMessage.newBuilder()
//                        .setMessageId(_timeStamp)
//                        .setDeviceid(_deviceId)
//                        .setChatInfo(chatInfoBuilder);
//
//        byte[] protobufSerialized = navitagMessageBuilder.build().toByteArray();
//
//        return serializedProtobufToPackage(protobufSerialized, false);
//    }

}
