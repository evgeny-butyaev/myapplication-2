package com.example.testprotobuf;

import android.location.Location;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class LocationDebug extends Location {

    public LocationDebug(String provider) {
        super(provider);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Field field : this.getClass().getDeclaredFields()) {
            String item;
            if(! field.getType().getSimpleName().equals("ArrayList")) {
                try {
                    Object value = field.get(this);
                    item = String.format("%s %s %s: %s%n", Modifier.toString(field.getModifiers()), field.getType().getSimpleName(), field.getName(), String.valueOf(value));
                    sb.append(item);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                item = String.format("%s %s %s: ArrayList<>%n", Modifier.toString(field.getModifiers()), field.getType().getSimpleName(), field.getName());
                sb.append(item);
            }
        }
        return sb.toString();
    }
}
