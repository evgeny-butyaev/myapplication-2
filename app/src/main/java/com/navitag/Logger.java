package com.navitag;

import com.navitag.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;

public class Logger {
    //private String m_logData = "";

    private static Logger m_instance;

    public static Logger instance () {
        if (m_instance == null)
            m_instance = new Logger(ServiceSettings.instance().getLogFileName());

        return m_instance;
    }

    private ServiceSettings settings() { return ServiceSettings.instance(); }

    private final String m_logFileName;
    private final String m_logErrorFileName;

    public Logger(String logFilename) {
        m_logFileName = logFilename;
        File fl = new File(logFilename);

        File crash_folder = new File(fl.getParent() + "/crash_info");

        crash_folder.mkdirs();
        m_logErrorFileName = crash_folder.getPath() + "/" + fl.getName();
    }

    public void logStats (String message)
    {
        if (settings().getLogBatteryState()) {
            String statsFileName = m_logFileName.replace(".", "Stats.");
            writeToLogFile(message, true, statsFileName);
        }
    }

    public void moveStatsFile()
    {
        try {
            String statsFileName = m_logFileName.replace(".", "Stats.");
            File statsFile = new File(statsFileName);

            if (statsFile.exists()) {
                String statsFileNewName = m_logFileName.replace(".", "Stats" + Utils.dateString(Utils.nowMS()) + ".");
                File newStatsFile = new File(statsFileNewName);

                if (!newStatsFile.exists())
                    statsFile.renameTo(newStatsFile);
            }
        }
        catch (Exception e)
        {
            logError(e);
        } catch (Error er) {
            logError(er);
        }
    }

    public void writeToLog(String message, boolean append, boolean isError) {
        writeToLogFile(message, append, isError ? m_logErrorFileName : m_logFileName);
    }

    public void writeToLogFile(String message, boolean append, String _fileName) {
        try {
            FileOutputStream outputStream = new FileOutputStream(new File(_fileName), append);

            String dateAndMessage = Utils.dateString(Utils.nowMS(), true, false, true, true, false)  + ": " + message;

            outputStream.write(dateAndMessage.getBytes());
            outputStream.close();
        } catch (Exception e) {
            int aaa = 1;
        } catch (Error er) {
            int aaa = 1;
           // er.printStackTrace();
        }
    }

    public synchronized void logLine(String message) {
        if (settings().getInCreateLogMode())
            logMessage(message, true);
    }

    private void logMessage(String message, boolean newLine) {
        String logMessage =  message + (newLine ? "\n" :"");
        writeToLog(logMessage, true, false);
    }

    public void logStack (String _message, int _nLines) {
        StackTraceElement[] traceElems = Thread.currentThread().getStackTrace();
        String strTrace = "";

        for (int i = 1; i <= _nLines && i < traceElems.length; ++i)
            strTrace += traceElems[i] + "\n";

        logLine(_message + " CALLER TRACE: \n" + strTrace);
    }

    public synchronized void logError (String msg, Throwable e) {
        logLine(msg);
        logError(e);
    }

    public void logError (Throwable e) {
        boolean isCrash = (e.getClass() == Error.class);

        if (settings().getInCreateLogMode() || isCrash) {
            StackTraceElement[] traceElems = e.getStackTrace();

            if (traceElems.length > 1)
                writeToLog(e.getClass().getName() + "EXCEPTION in " + traceElems[1].toString() + "(): " +
                                e.getMessage() + "\n",
                        true, isCrash);

            for (int i = 0; i < traceElems.length; ++i)
                writeToLog("   " + traceElems[i] + "\n", true, isCrash);
        }

        e.printStackTrace();
    }

    public StackTraceElement[] logStackTrace() {
        StackTraceElement[] traceElems = Thread.currentThread().getStackTrace();

        for (int i = 0; i < traceElems.length; ++i)
            writeToLog("   " + traceElems[i] + "\n", true, false);

        return traceElems;
    }

    public boolean moveLogFile (String strTmpFileName) {
        boolean bRet = false;

        File logFile = new File(m_logFileName);

        if (logFile.exists()) {
            File tmpFile = new File(strTmpFileName);

            if (!tmpFile.exists()) {
                logFile.renameTo(tmpFile);
                bRet = true;
            }
        }

        return  bRet;
    }
}
