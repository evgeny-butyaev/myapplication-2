package com.navitag;

import android.content.Context;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import org.osmdroid.util.GeoPoint;

/**
 * Created by eugene on 4/11/17.
 */
public class TrackerPoint implements Parcelable {
    static final float METER_PER_SECOND_TO_KM_PER_HOUR = 3.6f;

    public boolean m_sent = false;
    public long m_firstSentTimestamp = -1;

    public long timeSecondsFromEpoh;
    public double longitude;
    public double latitude;
    public double altitudeMeters;
    public float bearing;
    public float speedKmPerHour;
    public float accuracyMeters;
    public int maxSatellites;
    public int batteryPercent;
    public int batteryMv;
    public int batteryTemperatureDegreesCelsius;
    public float distanceFromLastLocation;
    public String provider = "";

    public TrackerPoint(
            Location _loc,
            Location _oldLoc,
            Context _ctx
    ) {
        this.timeSecondsFromEpoh = _loc.getTime() / 1000;// TimeUnit.MILLISECONDS.toSeconds(location.getTime());
        this.latitude = _loc.getLatitude();
        this.longitude = _loc.getLongitude();
        this.speedKmPerHour = _loc.getSpeed() * METER_PER_SECOND_TO_KM_PER_HOUR;
        this.bearing = _loc.getBearing();
        this.altitudeMeters = _loc.getAltitude();
        this.accuracyMeters = _loc.getAccuracy();
        this.maxSatellites = _loc.getExtras() != null && _loc.getExtras().containsKey("satellites") ?
                _loc.getExtras().getInt("satellites") : 0;
        this.batteryPercent = DeviceInfoManager.getBatteryPercentage(_ctx);
        this.batteryMv = DeviceInfoManager.getVoltageMv(_ctx);
        this.batteryTemperatureDegreesCelsius =
                DeviceInfoManager.getTemperatureDegreesCelsius(_ctx);
        this.distanceFromLastLocation = (_oldLoc != null) ? _loc.distanceTo(_oldLoc) : 0;
        this.provider = _loc.getProvider();
    }

    public TrackerPoint(Parcel in) {
        readFromParcel(in);
    }

    public GeoPoint getGeoPoint() {
        return new GeoPoint(latitude, longitude);
    }

    public static final Parcelable.Creator<TrackerPoint> CREATOR =
            new Parcelable.Creator<TrackerPoint>() {
                public TrackerPoint createFromParcel(Parcel source) {
                    return new TrackerPoint(source);
                }

                public TrackerPoint[] newArray(int size) {
                    throw new UnsupportedOperationException();
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(m_sent ? (byte) 1 : (byte) 0);
        dest.writeLong(timeSecondsFromEpoh);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeDouble(altitudeMeters);
        dest.writeFloat(bearing);
        dest.writeFloat(speedKmPerHour);
        dest.writeFloat(accuracyMeters);
        dest.writeInt(maxSatellites);
        dest.writeInt(batteryPercent);
        dest.writeInt(batteryMv);
        dest.writeInt(batteryTemperatureDegreesCelsius);
        dest.writeFloat(distanceFromLastLocation);
        dest.writeString(provider);
    }

    private void readFromParcel(Parcel in) {
        m_sent = in.readByte() == 1;
        timeSecondsFromEpoh = in.readLong();
        longitude = in.readDouble();
        latitude = in.readDouble();
        altitudeMeters = in.readDouble();
        bearing = in.readFloat();
        speedKmPerHour = in.readFloat();
        accuracyMeters = in.readFloat();
        maxSatellites = in.readInt();
        batteryPercent = in.readInt();
        batteryMv = in.readInt();
        batteryTemperatureDegreesCelsius = in.readInt();
        distanceFromLastLocation = in.readFloat();
        provider = in.readString();
    }

    public boolean setSentIfInPackage(long _ts) {
        if (_ts == m_firstSentTimestamp)
            m_sent = true;

        return m_sent;
    }

    public void setFirstSentTimestamp(long _ts) {
        m_firstSentTimestamp = _ts;
    }
}
