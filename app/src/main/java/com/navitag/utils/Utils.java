package com.navitag.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
//import android.support.v4.app.ActivityCompat;
import android.text.format.DateUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.navitag.Logger;
//import com.navitag.R;
import com.navitag.ServiceSettings;
//import com.navitag.app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by eugene on 8/22/16.
 */
public class Utils {
    private final static int DAYS_TO_CHANGE_ELAPSED_DATE_OUTPUT = 7;

    private static ServiceSettings settings() {
        return ServiceSettings.instance();
    }

    private static Logger logger() {
        return Logger.instance();
    }

    public static double getDoubleSafe(JSONObject _json, String columnName) {
        double ret = -1;

        try {
            ret = _json.getDouble(columnName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static long getLongSafe(JSONObject _json, String columnName) {
        long ret = 0;

        try {
            ret = _json.getLong(columnName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }


    public static String getStringSafe(JSONObject _json, String columnName) {
        String ret = "";

        try {
            ret = _json.getString(columnName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static JSONObject getJSONObjectSafe(JSONObject _json, String columnName) {
        JSONObject ret = null;

        try {
            ret = _json.getJSONObject(columnName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }


    public static double getDoubleSafe(JSONArray _jsonArr, int _idx) {
        double ret = -1;

        try {
            ret = _jsonArr.getDouble(_idx);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static long getLongSafe(JSONArray _jsonArr, int _idx) {
        long ret = 0;

        try {
            ret = _jsonArr.getLong(_idx);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static String getStringSafe(JSONArray _jsonArr, int _idx) {
        String ret = "";

        try {
            ret = _jsonArr.getString(_idx);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static long nowMS() {
        return (new Date()).getTime();
    }

    private static SimpleDateFormat _getFormat (String _format, boolean _utc) {
        SimpleDateFormat ret = new SimpleDateFormat(_format);
        if (_utc)
            ret.setTimeZone(TimeZone.getTimeZone("UTC"));

        return ret;
    }

    public static String dateString(
            long _timeMS,
            boolean _showYear,
            boolean _monthAsString,
            boolean _alwaysLong,
            boolean _showSeconds,
            boolean _utc) {

        String yearFormat = _showYear ? "yy." : "";
        String days = _alwaysLong || Utils.nowMS() - _timeMS > 86400 * 1000 ?
                _getFormat(_monthAsString ? yearFormat + "d MMM " : "MM.dd ", _utc).format(new Date(_timeMS)) : "";
        String hours = _getFormat(_showSeconds ? "HH:mm:ss" : "HH:mm", _utc).format(new Date(_timeMS));

        return (days + hours);
    }

//    public static String dateStringMonth3(long _timeMS, boolean _utc) { return dateString(_timeMS, true, false, true, _utc); }
    public static String dateString(long _timeMS, boolean _utc) { return dateString(_timeMS, true, false, true, true, _utc); }
    public static String dateString(long _timeMS) {
        return dateString(_timeMS, false);
    }

    public static String getElapsedTimeString(long _msMomentInPast) {
        String ret = "";

        long msNow = nowMS();
        long difference = msNow - _msMomentInPast;
        long differenceDays = difference / (1000 * 60 * 60 * 24);

        if (differenceDays < DAYS_TO_CHANGE_ELAPSED_DATE_OUTPUT) {
            final CharSequence relativeTimeSpanString =
                    DateUtils.getRelativeTimeSpanString(
                            _msMomentInPast, msNow,
                            DateUtils.MINUTE_IN_MILLIS
                    );
            ret = relativeTimeSpanString.toString();

        } else
            ret = DateFormat.getDateTimeInstance().format(_msMomentInPast);

        return ret;
    }

//    public static String getDurationString (
//            Resources _res,
//            long _totalSeconds
//    ) {
//        long daysTotal = TimeUnit.SECONDS.toDays(_totalSeconds);
//        long years = daysTotal / 365;
//        long months = (daysTotal - years * 365) / 30;
//        long days = daysTotal - years * 365 - months * 30;
//        long hours = TimeUnit.SECONDS.toHours(_totalSeconds) - (daysTotal * 24);
//        long minutes = TimeUnit.SECONDS.toMinutes(_totalSeconds) - (TimeUnit.SECONDS.toHours(_totalSeconds) * 60);
//
//        if (years > 0) {
//            return String.format("%d %s %d %s",
//                    years, _res.getString(R.string.years_bref),
//                    months, _res.getString(R.string.months_bref));
//        } else if (months > 0) {
//            return String.format("%d %s %d %s",
//                    months, _res.getString(R.string.months_bref),
//                    days, _res.getString(R.string.days_bref));
//        } else if (days > 0) {
//            return String.format("%d %s %d %s",
//                    days, _res.getString(R.string.days_bref),
//                    hours, _res.getString(R.string.hours_bref));
//        } else if (hours > 0) {
//            return String.format("%d %s %d %s",
//                    hours, _res.getString(R.string.hours_bref),
//                    minutes, _res.getString(R.string.minutes_bref));
//        } else
//            return String.format("%d %s",
//                    minutes, _res.getString(R.string.minutes_bref));
//    }

//    public static String getLengthString (Context _ctx, double _len) {
//        return String.format(Locale.getDefault(), "%.1f ", _len)  + _ctx.getString(R.string.km);
//    }

    public static void hideKeyboard (View _v) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) _v.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(_v.getWindowToken(), 0);
    }

//    public static boolean hasPermissions(Context context, String... permissions) {
//        if (Utils.isMarshmellowOrHigher()) {
//            if (context != null && permissions != null)
//                for (String permission : permissions)
//                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
//                        return false;
//        }
//
//        return true;
//    }
//
//    public static Uri getPackageUri () {
//        String pkg= "package:"+ app.getContext().getPackageName() ;
//        return Uri.parse(pkg);
//    }

    public static boolean isMarshmellowOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isOreoOrHigher () {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static boolean isJellyBeanOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isNougatOrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }
}
