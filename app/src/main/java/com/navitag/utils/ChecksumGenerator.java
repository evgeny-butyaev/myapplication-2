package com.navitag.utils;

public class ChecksumGenerator {

    private int crc16_ch(int crc, char b) {
        int i = 8;
        crc ^= (int) b << 8;
        do {
            if ((crc & 0x8000) > 0) {
                crc <<= 1;
                crc ^= 0x1021;
            } else {
                crc <<= 1;
            }
        } while (--i > 0);
        return crc;
    }

    public short getCrc16(byte[] array) {
        int crc = 0;
        int arraySize = array.length;

        int t = 0;
        while (arraySize-- > 0) {
            crc = crc16_ch(crc, (char) array[t++]);
        }

        return (short) crc;
    }
}
