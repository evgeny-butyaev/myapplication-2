package com.navitag;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;


public class DeviceInfoManager {
    static public int getTemperatureDegreesCelsius (Intent _intent) {
        return _intent != null ?
                _intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1) / 10 : 0;
    }

    static public int getVoltageMv(Intent _intent) {
        return _intent != null ?
                _intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1) : 0;
    }

    static public int getTemperatureDegreesCelsius(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, ifilter);
        if (intent == null) {
            Log.e("getTemperature", "ACTION_BATTERY_CHANGED registerReceiver failed");
            return 0;
        }

        return getTemperatureDegreesCelsius(intent);
    }

    static public int getVoltageMv(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, ifilter);
        if (intent == null) {
            Log.e("getVoltageMv", "ACTION_BATTERY_CHANGED registerReciever failed");
            return -1;
        }
        return getVoltageMv (intent);
    }

    static public int getBatteryPercentage(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        if (batteryStatus == null) {
            Log.e("getBatteryPercentage", "getting battery status failed");
            return 0;
        }

        return batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }

    static public boolean isPlugged (Context context, Logger _logger) {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, ifilter);

        if (intent == null) {

            _logger.logLine("isPlugged(): getting battery status failed");
            return false;
        }

        boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, true);
        int status = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

        boolean bPluggedAC = (status == BatteryManager.BATTERY_PLUGGED_AC);
        boolean bPluggedUSB = (status == BatteryManager.BATTERY_PLUGGED_USB);
        boolean bPluggedWireless = (status == BatteryManager.BATTERY_PLUGGED_WIRELESS);

        int voltage = getVoltageMv(intent);
        int temperature = (getTemperatureDegreesCelsius(context));

        _logger.logLine("isPlugged() : PLUGGED_AC/PLUGGED_USB/PLUGGED_WIRELESS/T/Voltage: " +
                bPluggedAC + "/" + bPluggedUSB + "/" + bPluggedWireless + "/" +
                temperature + "/" + voltage );

        return !present ||
                (temperature == 0 && voltage == 0) ||
                bPluggedAC || bPluggedUSB || bPluggedWireless;
    }

    static public boolean isCharging(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        if (batteryStatus == null) {
            Log.e("isCharging", "getting battery status failed");
            return false;
        }

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        return status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

    }

    static public boolean isPluggedAlways(Context context, Logger _logger) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, ifilter);

        if (intent == null) {
            _logger.logLine("isPluggedAlways(): getting battery status failed");
            return false;
        }


        boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, true);

        _logger.logLine("CHECKING PLUGGED ALWAYS REASON.");
        _logger.logLine(
                "BATTERY PRESENT/TEMP/VOLTAGE: " + present + "/" +
                    getTemperatureDegreesCelsius(intent) + "/" + getVoltageMv(intent));

        boolean bRet = !present ||
                        (getTemperatureDegreesCelsius(intent) == 0 &&
                        getVoltageMv(intent) == 0);

        return bRet;
    }
}
