package com.navitag;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.text.TextUtils;

import com.example.testprotobuf.BuildConfig;
import com.navitag.API.CredentialsInfo;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;


public class ServiceSettings {

    private Logger logger() {
        return Logger.instance();
    }

    private ServiceSettings() {
    }

    private static ServiceSettings m_instance = null;

    public static ServiceSettings instance() {
        if (m_instance == null)
            m_instance = new ServiceSettings();

        return m_instance;
    }

    ////////////////////////////////////////////////////////////////
    private static final String PREFS_NAME = "NavitelTrackerSettings";

    public boolean getUseDisk() {
        return true;
    }

    public boolean isEraGlonass() {
        return true;
    }

    public boolean showMenu() {
        return true;
    }

    public boolean getInCreateLogMode() { return false; }
    public boolean getLogBatteryState() {
        return false;
    }
    public boolean getLogPoints() {
        return false;
    }
    public boolean logAllLocations () { return false; }
    public boolean collectStats () { return false; }
    public boolean getSendLogMails() {
        return false;
    }

    public boolean supportAssistanceRequests() {
        return isEraGlonass();
    }

    public boolean supportChat() {
        return isEraGlonass();
    }

    private boolean m_showConnectionSettings = false;
    public boolean showConnectionSettings() {
        return m_showConnectionSettings;
    }

    private boolean m_showChatNotifications = true;
    public boolean getShowChatNotifications() {
        return m_showChatNotifications;
    }
    public void setShowChatNotifications(boolean _show) {
        m_showChatNotifications = _show;
    }

    private String[] m_requiredPermissions = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public String[] getRequiredPermissions () { return m_requiredPermissions; }

    private int m_savedAppVersion = 27;
    private final int m_minRequiredSettingsVersion = 18;
    private final int m_savedAppVersionDef = 27;

    private String m_httpServerAddressDef = "monitoring.aoglonass.ru";
//    private int m_serverPortDef = 19990;

    private String m_serverAddressDef = "monitoring.aoglonass.ru";
    //private String m_serverAddressDef = "dp-test05.navitel.cnt";
    private int m_serverPortDef = 30159;

    private ArrayList<String> m_statusesDef = new ArrayList<String>();
    private String m_statusDef = "";
    private boolean m_reportStatusesDef = true;

//    private String m_serverAddressDef = "tracker.dispatch.navitel.su";
//    private int m_serverPortDef = 30206;


//    private int m_serverPortDef = 40160;//20159;
// private String m_serverAddressDef = "192.168.139.109";

    /////////////////////////////////////////////////////////////////
//    private String m_httpServerRootUrl = "https://www.dispatch.navitel.ru";
//    public String getHttpServerRootUrl() { return m_httpServerRootUrl + "/rpc"; }

    public String getHttpServerRootUrl() {
        return //"http://dp-test03.navitel.cnt/rpc";
                "https://" + m_httpServerAddressDef + "/rpc";
    }

    private String m_apiKey = null;

    public void setApiKey(String _val) {
        m_apiKey = _val;
    }

    public String getApiKey() {
        return m_apiKey;
    }

    public boolean isPhotoPermitted() {
        return true;
    }

    public boolean isLocationProviderWarningPermitted() {
        return true;
    }

    private CredentialsInfo m_credentials = null;

    public void setCredentials(CredentialsInfo _cred) {
        m_credentials = _cred;
    }

    public CredentialsInfo getCredentials() {
        return m_credentials;
    }

    /////////////////////////////////////////////////////////////////
    private boolean m_signedUp = false;

    public boolean getSignedUp() {
        return m_signedUp;
    }

    public void setSignedUp(boolean _val) {
        m_signedUp = _val;
    }

    public long getKeepSocketAliveAfterAckMS() {
        return 2 * 60 * 1000;
    }

    private int m_signUpPostponeSeconds = -1;
    public int getSignUpPostponeSeconds() {
        return m_signUpPostponeSeconds;
    }
    public void setSignUpPostponeSeconds(int _val) {
        m_signUpPostponeSeconds = _val;
    }

    private long m_lLastSignUpPromptMS = -1;
    public long getLastSignUpPromptMS() {
        return m_lLastSignUpPromptMS;
    }
    public void setLastSignUpPromptMS(long _val) {
        m_lLastSignUpPromptMS = _val;
    }

    private long m_lastUnrestrictBackgroundDataInternetAccessNotificationMS = 0;
    public void setLastUnrestrictBackgroundDataInternetAccessNotificationMS  (long _val) { m_lastUnrestrictBackgroundDataInternetAccessNotificationMS = _val; }
    public long getLastUnrestrictBackgroundDataInternetAccessNotificationMS () { return m_lastUnrestrictBackgroundDataInternetAccessNotificationMS; }

    private Location m_lastLocation = null;
    public Location getLastLocation () { return m_lastLocation; }
    public void setLastLocation (Location _val) { m_lastLocation = _val; }

    private long m_lastLocationUpdateMS = -1;
    public long getLastLocationUpdateMS() { return m_lastLocationUpdateMS; }
    public void setLastLocationUpdateMS(long _val) { m_lastLocationUpdateMS = _val;}

    private ArrayList<String> m_enabledProviders = new ArrayList<>();
    public boolean wasProviderEnabled (String _p) { return m_enabledProviders.contains(_p);}
    public void addEnabledProvider (String _p) {
        if (!m_enabledProviders.contains(_p));
        m_enabledProviders.add(_p);
    }
    public void removeEnabledProvider (String _p) {
        if (m_enabledProviders.contains(_p))
            m_enabledProviders.remove(_p);
    }

    /////////////////////////////////////////////////////////////////
    private final int[] m_sendIntervalSecDef = {1200, 0};
    private final int[] m_updateDistanceMDef = {5, 1};
    private final int[] m_updatePeriodSecDef = {5, 1};

    private final int[] m_filterTimeoutSecDef = {15, 15};

    private final boolean[] m_useNetworkSourceDef = {false, false};
    private final boolean[] m_useGpsSourceDef = {true, true};
    //private final boolean m_useAccelerometerControlDef = false;
    private final int m_savePeriodSecDef = 600;
    private final int m_shakeThresholdDef = 600;
    private final int m_disableGpsAfterSecDef = 60;
    private final String m_logFilenameDef = "/sdcard/eraTracker.log";
    private final String m_gpxFilenameDef = "/sdcard/gpx.log";
    private final String m_fatalErrorLogDef = "/sdcard/nav_tracker/crash.log";
    private final String m_statsFileNameDef = "navTrackerStats.log";

    private final boolean m_useScheduleDef = false;
    private final int m_fromHourDef = 0;
    private final int m_toHourDef = 23;
    private final int m_fromMinuteDef = 0;
    private final int m_toMinuteDef = 0;
    private final int m_scheduleDaysFlagDef = (1 | 2 | 4 | 8 | 16 | 32 | 64);

    private final boolean m_usePinDef = false;
    private final String m_pinDef = "";
    private Timer m_forgetPinProvidedTimerTask = null;

    private final boolean m_showServiceIconDef = true;
    private final String m_mailFromDef = "eracontrol.log@navitel.su";
    private final String[] m_mailToDef = {"bv345kjhvfhdgvf@navitel.su"};
    private final String m_mailSubjectDef = "UNKNOWN device";
    private final String m_mailBodyDef = "Cheers!\n\n Here is an EMPTY mail.\n\n Yours respectfully, NaviTag Log";

    /////////////////////////////////////////////////////////////////
    private String m_serverAddress = m_serverAddressDef;
    private int m_serverPort = m_serverPortDef;

    private int[] m_sendIntervalSec =
            {m_sendIntervalSecDef[0], m_sendIntervalSecDef[1]};
    private int[] m_updateDistanceM =
            {m_updateDistanceMDef[0], m_updateDistanceMDef[1]};
    private int[] m_updatePeriodSec =
            {m_updatePeriodSecDef[0], m_updatePeriodSecDef[1]};
    private int[] m_filterTimeoutSec =
            {m_filterTimeoutSecDef[0], m_filterTimeoutSecDef[1]};
    private boolean[] m_useNetworkSource =
            {m_useNetworkSourceDef[0], m_useNetworkSourceDef[1]};
    private boolean[] m_useGpsSource =
            {m_useGpsSourceDef[0], m_useGpsSourceDef[1]};
    //private boolean m_useAccelerometerControl = m_useAccelerometerControlDef;


    private boolean m_useSchedule = m_useScheduleDef;
    private int m_fromHour = m_fromHourDef;
    private int m_toHour = m_toHourDef;
    private int m_fromMinute = m_fromMinuteDef;
    private int m_toMinute = m_toMinuteDef;
    private int m_scheduleDaysFlag = m_scheduleDaysFlagDef;

    private long m_gpsPhoneTimeDiff = 0;

    private boolean m_usePin = m_usePinDef;
    private String m_pin = m_pinDef;
    private boolean m_pinProvided = false;

    private int m_savePeriodSec = m_savePeriodSecDef;
    private int m_shakeThreshold = m_shakeThresholdDef;
    private int m_disableGpsAfterSec = m_disableGpsAfterSecDef;
    private String m_logFilename = m_logFilenameDef;
    private String m_gpxFilename = m_gpxFilenameDef;
    private String m_fatalErrorLog = m_fatalErrorLogDef;
    private String m_statsFileName = m_statsFileNameDef;
    private boolean m_showServiceIcon = m_showServiceIconDef;

    private final String m_mailFrom = m_mailFromDef;
    private final String[] m_mailTo = m_mailToDef;
    private final String m_mailSubject = m_mailSubjectDef;
    private final String m_mailBody = m_mailBodyDef;

    public int getMinRequiredVersion() {
        return m_minRequiredSettingsVersion;
    }

    public void setSavedAppVersion(int _version) {
        m_savedAppVersion = _version;
    }

    public int getSavedAppVersion() {
        return m_savedAppVersion;
    }

    public void setServerAddress(String serverAddress) {
        m_serverAddress = serverAddress;
    }

    public String getServerAddress() {
        return m_serverAddress;
    }

    public void setServerPort(int serverPort) {
        m_serverPort = serverPort;
    }

    public int getServerPort() {
        return m_serverPort;
    }

    private boolean m_reportStatuses = m_reportStatusesDef;
    private ArrayList<String> m_statuses = m_statusesDef;
    private boolean m_statusesChanged = false;

    public void setReportStatuses(boolean _send) {
        m_reportStatuses = _send;
    }

    public boolean getReportStatuses() {
        return isEraGlonass() && m_reportStatuses;
    }

    public void setStatuses(List<String> _statuses) {
        m_statuses = new ArrayList<String>(_statuses);
    }

    public String[] getStatuses() {
        return m_statuses != null ? //&& m_statuses.size() > 0 ?
                m_statuses.toArray(new String[m_statuses.size()]) :
                null;
    }

    public void setStatusesChanged(boolean _changed) {
        m_statusesChanged = _changed;
    }

    public boolean getStatusesChanged() {
        return m_statusesChanged;
    }

    private int m_selectedStatusIdx = -1;

    public int getSelectedStatusIdx() {
        return m_selectedStatusIdx;
    }

    public void setSelectedStatusIdx(int _idx) {
        m_selectedStatusIdx = _idx;
    }

    public String getStatus() {
        return
                m_selectedStatusIdx >= 0 && getStatuses().length > m_selectedStatusIdx ?
                        getStatuses()[m_selectedStatusIdx] : m_statusDef;
    }

    public void setSendIntervalSec(boolean plugged, int timeout) {
        m_sendIntervalSec[plugged ? 1 : 0] = timeout;
    }

    public int getSendIntervalSec(boolean plugged) {
        return m_sendIntervalSec[plugged ? 1 : 0];
    }

    public void setUpdateDistanceM(boolean plugged, int M) {
        m_updateDistanceM[plugged ? 1 : 0] = M;
    }

    public int getUpdateDistanceM(boolean plugged) {
        return m_updateDistanceM[plugged ? 1 : 0];
    }

    public void setUpdatePeriodSec(boolean plugged, int Sec) {
        m_updatePeriodSec[plugged ? 1 : 0] = Sec;
    }

    public int getUpdatePeriodSec(boolean plugged) {
        return m_updatePeriodSec[plugged ? 1 : 0];
    }

    public void setFilterPeriodSec(boolean plugged, int Sec) {
        m_filterTimeoutSec[plugged ? 1 : 0] = Sec;
    }

    public int getFilterPeriodSec(boolean plugged) {
        return m_filterTimeoutSec[plugged ? 1 : 0];
    }

    public void setUseNetworkSource(boolean plugged, boolean CanUse) {
        m_useNetworkSource[plugged ? 1 : 0] = CanUse;
    }

    public boolean getUseNetworkSource(boolean plugged) {
        return m_useNetworkSource[plugged ? 1 : 0];
    }

    public boolean useNetworkProviderAlongFused() {
        return true;
    }

    public void setUseGpsSource(boolean _plugged, boolean CanUse) {
        m_useGpsSource[_plugged ? 1 : 0] = CanUse;
    }

    public boolean getUseGpsSource(boolean _plugged) {
        return m_useGpsSource[_plugged ? 1 : 0];
    }

    public void setShakeThreshold(int threshold) {
        m_shakeThreshold = threshold;
    }

    public int getShakeThreshold() {
        return m_shakeThreshold;
    }

    public void setDisableGpsAfterSec(int Sec) {
        m_disableGpsAfterSec = Sec;
    }

    public int getDisableGpsAfterSec() {
        return m_disableGpsAfterSec;
    }

    public void setLogFilename(String logFilename) {
        m_logFilename = logFilename;
    }

    public String getLogFileName() {
        return m_logFilename;
    }

    public boolean getDisplayColorScheduleStatus() {
        return false;
    }

    public boolean getUseSchedule() {
        return m_useSchedule;
    }

    public int getFromHour() {
        return m_fromHour;
    }

    public int getToHour() {
        return m_toHour;
    }

    public int getFromMinute() {
        return m_fromMinute;
    }

    public int getToMinute() {
        return m_toMinute;
    }

    public int getScheduleDaysFlag() {
        return m_scheduleDaysFlag;
    }

    public void setUseSchedule(boolean _val) {
        m_useSchedule = _val;
    }

    public void setFromHour(int _val) {
        m_fromHour = _val;
    }

    public void setToHour(int _val) {
        m_toHour = _val;
    }

    public void setFromMinute(int _val) {
        m_fromMinute = _val;
    }

    public void setToMinute(int _val) {
        m_toMinute = _val;
    }

    public void setScheduleDaysFlag(int _val) {
        m_scheduleDaysFlag = _val;
    }

    public long getGpsPhoneTimeDiff () {
        return m_gpsPhoneTimeDiff;
    }

    public void setGpsPhoneTimeDiff (long _diff) {
        m_gpsPhoneTimeDiff = _diff;
    }

    public void setUsePin(boolean _val) {
        m_usePin = _val;
    }

    public boolean getUsePin() {
        return m_usePin;
    }

    public void setPin(String _val) {
        m_pin = _val;
    }

    public String getPin() {
        return m_pin;
    }

    public void setPinProvided(boolean _val) {
        m_pinProvided = _val;
        if (m_pinProvided)
            restartForgetPinProvidedTimer();
    }

    public boolean getPinProvided() {
        return m_pinProvided;
    }

    public void setShowServiceIcon(boolean _val) {
        m_showServiceIcon = _val;
    }

    public boolean getShowServiceIcon() {
        return m_showServiceIcon;
    }


    public String[] getMailTo() {
        return m_mailTo;
    }

    public String getMailFrom() {
        return m_mailFrom;
    }

    public String getMailSubject() {
        return m_mailSubject;
    }

    public String getMailBody() {
        return m_mailBody;
    }

    public void logSettings(boolean bSaving) {

        if (ServiceSettings.instance().getInCreateLogMode()) {

            logger().logLine(" ************ " +
                    (bSaving ? " SAVING SETTINGS " : " RETRIEVING SETTINGS ") +
                    " ************ "
            );

            StackTraceElement[] traceElems = Thread.currentThread().getStackTrace();

            String strLineToLog = " CALLER TRACE:  \n";

            for (int i = 0; i < Math.min(traceElems.length, 8); ++i)
                strLineToLog += traceElems[i] + "\n";

            strLineToLog +=
                    "serverAddress: " + getServerAddress() + "\n" +
                    "serverPort: " + getServerPort() + "\n" +

                    "send statuses: " + getReportStatuses() + "\n" +
                    "statuses: " + getStatuses() + "\n" +
                    "selectedStatusIdx: " + getSelectedStatusIdx() + "\n" +

                    "sendIntervalUnplugged: " + getSendIntervalSec(false) + "\n" +
                    "updateDistanceUnpluggedM: " + getUpdateDistanceM(false) + "\n" +
                    "updatePeriodUnpluggedSec: " + getUpdatePeriodSec(false) + "\n" +
                    "filterPeriodUnpluggedSec: " + getFilterPeriodSec(false) + "\n" +

                    "sendIntervalPlugged: " + getSendIntervalSec(true) + "\n" +
                    "updateDistancePluggedM: " + getUpdateDistanceM(true) + "\n" +
                    "updatePeriodPluggedSec: " + getUpdatePeriodSec(true) + "\n" +
                    "filterPeriodPluggedSec: " + getFilterPeriodSec(true) + "\n" +

                    "useNetworkSourceUnplugged: " + getUseNetworkSource(false) + "\n" +
                    "useGpsSourceUnplugged: " + getUseGpsSource(false) + "\n" +
                    //"useAccelerometerControl: " + getUseAccelerometerControl() + "\n" +

                    "useNetworkSourcePlugged: " + getUseNetworkSource(true) + "\n" +
                    "useGpsSourcePlugged: " + getUseGpsSource(true) + "\n" +
                    //"useAccelerometerControl: " + getUseAccelerometerControl() + "\n" +
                    "shakeThreshold: " + getShakeThreshold() + "\n" +
                    "disableGpsAfterTimeout: " + getDisableGpsAfterSec() + "\n" +
                    "logFilename: " + getLogFileName() + "\n" +
                    "setUseSchedule: " + getUseSchedule() + "\n" +
                    "setFromHour: " + getFromHour() + "\n" +
                    "setFromMinute: " + getFromMinute() + "\n" +
                    "setToHour: " + getToHour() + "\n" +
                    "setToMinute: " + getToMinute() + "\n" +
                    "scheduleDaysFlag: " + getScheduleDaysFlag();

            logger().logLine(strLineToLog);
        }
    }

    private void saveCurrentVersion(SharedPreferences.Editor editor) {
        editor.putInt("savedAppVersion", BuildConfig.VERSION_CODE);
    }

//    public void saveLastLocationUpdateTime (Context context) {
//        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putLong("last_location_update_ms", getLastLocationUpdateMS());
//        NaviTagService.saveSharedPreferences(editor);
//    }

//    public void saveSettings() {
//        saveSettings(true);
//    }

//    public void saveSettings(boolean _saveLog) {
//        try {
//
//            SharedPreferences settings = app.getContext().getSharedPreferences(PREFS_NAME, 0);
//            SharedPreferences.Editor editor = settings.edit();
//
//            saveCurrentVersion(editor);
//
//            editor.putBoolean("signed_up", getSignedUp());
//            editor.putLong("last_signup_prompt_ms", getLastSignUpPromptMS());
//            editor.putInt("signup_postpone_sec", getSignUpPostponeSeconds());
//            editor.putLong("last_unrestrict_background_data_ms", getLastUnrestrictBackgroundDataInternetAccessNotificationMS());
//
//            editor.putLong("last_location_update_ms", getLastLocationUpdateMS());
//
//            editor.putBoolean("useSchedule", getUseSchedule());
//            editor.putInt("fromHour", getFromHour());
//            editor.putInt("fromMinute", getFromMinute());
//            editor.putInt("toHour", getToHour());
//            editor.putInt("toMinute", getToMinute());
//            editor.putInt("scheduleDaysFlag", getScheduleDaysFlag());
//
//            editor.putBoolean("usePin", getUsePin());
//            editor.putString("pin", getPin());
//
//            editor.putBoolean("showServiceIcon", getShowServiceIcon());
//
//            editor.putString("serverAddress", getServerAddress());
//            editor.putInt("serverPort", getServerPort());
//
//            editor.putBoolean("sendStatuses", getReportStatuses());
//            List<String> statusesLengths = new ArrayList<>();
//            for (String status : m_statuses)
//                statusesLengths.add(String.valueOf(status.length()));
//
//            editor.putString("statuses_lengths", TextUtils.join(",", statusesLengths));
//            editor.putString("statuses", TextUtils.join("", m_statuses));
//            editor.putInt("selectedStatusIdx", m_selectedStatusIdx);
//
//            editor.putInt("sendIntervalUnplugged", getSendIntervalSec(false));
//            editor.putInt("updateDistanceUnpluggedM", getUpdateDistanceM(false));
//            editor.putInt("updatePeriodUnpluggedSec", getUpdatePeriodSec(false));
//            editor.putInt("filterPeriodUnpluggedSec", getFilterPeriodSec(false));
//
//            editor.putInt("sendIntervalPlugged", getSendIntervalSec(true));
//            editor.putInt("updateDistancePluggedM", getUpdateDistanceM(true));
//            editor.putInt("updatePeriodPluggedSec", getUpdatePeriodSec(true));
//            editor.putInt("filterPeriodPluggedSec", getFilterPeriodSec(true));
//
//            editor.putBoolean("useNetworkSourceUnplugged", getUseNetworkSource(false));
//            editor.putBoolean("useGpsSourceUnplugged", getUseGpsSource(false));
//            // editor.putBoolean("useAccelerometerControl", getUseAccelerometerControl());
//
//            editor.putBoolean("useNetworkSourcePlugged", getUseNetworkSource(true));
//            editor.putBoolean("useGpsSourcePlugged", getUseGpsSource(true));
//
//            editor.putInt("shakeThreshold", getShakeThreshold());
//            editor.putInt("disableGpsAfterTimeout", getDisableGpsAfterSec());
//            editor.putString("logFilename", getLogFileName());
//
//            NaviTagService.saveSharedPreferences(editor);
//            if (_saveLog)
//                logSettings(true);
//            restartForgetPinProvidedTimer();
//        } catch (Exception e) {
//            logger().logError(e);
//        }
//    }

//    public void restoreDefaultSettings() {
//        SharedPreferences settings = app.getContext().getSharedPreferences(PREFS_NAME, 0);
//        SharedPreferences.Editor editor = settings.edit();
//
//        saveCurrentVersion(editor);
//
//        // don't change signup info
//
//        editor.putBoolean("service_started_once", false);
//        editor.putString("deviceId", "");
//        editor.putString("serverAddress", m_serverAddressDef);
//        editor.putInt("serverPort", m_serverPortDef);
//
//        setLastSignUpPromptMS(settings.getLong("last_signup_prompt_ms", 0));
//        setSignUpPostponeSeconds(settings.getInt("signup_postpone_sec", 0));
//        setLastUnrestrictBackgroundDataInternetAccessNotificationMS(settings.getLong("unrestrict_background_data_ms", 0));
//
//        editor.putBoolean("sendStatuses", m_reportStatusesDef);
//        editor.putString("statuses_lengths", "");
//        editor.putString("statuses", TextUtils.join ("", m_statusesDef));
//        editor.putInt("selectedStatusIdx", m_selectedStatusIdx);
//
//        editor.putString("logFilename", m_logFilenameDef);
//
//        editor.putBoolean("useSchedule", m_useScheduleDef);
//        editor.putInt("fromHour", m_fromHourDef);
//        editor.putInt("fromMinute", m_fromMinuteDef);
//        editor.putInt("toHour", m_toHourDef);
//        editor.putInt("toMinute", m_toMinuteDef);
//        editor.putInt("scheduleDaysFlag", m_scheduleDaysFlagDef);
//
//        editor.putBoolean("usePin", m_usePinDef);
//        editor.putString("pin", m_pinDef);
//
//        editor.putBoolean("showServiceIcon", m_showServiceIconDef);
//
//        editor.putInt("sendIntervalUnplugged", m_sendIntervalSecDef[0]);
//        editor.putInt("updateDistanceUnpluggedM", m_updateDistanceMDef[0]);
//        editor.putInt("updatePeriodUnpluggedSec", m_updatePeriodSecDef[0]);
//        editor.putInt("filterPeriodUnpluggedSec", m_filterTimeoutSecDef[0]);
//        editor.putBoolean("useNetworkSourceUnplugged", m_useNetworkSourceDef[0]);
//        editor.putBoolean("useGpsSourceUnplugged", m_useGpsSourceDef[0]);
//
//        editor.putInt("sendIntervalPlugged", m_sendIntervalSecDef[1]);
//        editor.putInt("updateDistancePluggedM", m_updateDistanceMDef[1]);
//        editor.putInt("updatePeriodPluggedSec", m_updatePeriodSecDef[1]);
//        editor.putInt("filterPeriodPluggedSec", m_filterTimeoutSecDef[1]);
//        editor.putBoolean("useNetworkSourcePlugged", m_useNetworkSourceDef[1]);
//        editor.putBoolean("useGpsSourcePlugged", m_useGpsSourceDef[1]);
//
//        //editor.putBoolean("useAccelerometerControl", m_useAccelerometerControlDef);
//        editor.putInt("shakeThreshold", m_shakeThresholdDef);
//        editor.putInt("disableGpsAfterTimeout", m_disableGpsAfterSecDef);
//
//
//        NaviTagService.saveSharedPreferences(editor);
//        restoreSettingsFromRegistry();
//        logSettings(true);
//    }
//
//    public void restoreSettingsFromRegistry() {
//        try {
//            SharedPreferences settings = app.getContext().getSharedPreferences(PREFS_NAME, 0);
//
//            setSignedUp(settings.getBoolean("signed_up", false));
//            setLastSignUpPromptMS(settings.getLong("last_signup_prompt_ms", -1));
//            setSignUpPostponeSeconds(settings.getInt("signup_postpone_sec", -1));
//            setLastUnrestrictBackgroundDataInternetAccessNotificationMS(settings.getLong("unrestrict_background_data_ms", -1));
//
//            setLastLocationUpdateMS(settings.getLong("last_location_update_ms", -1));
//            setSavedAppVersion(settings.getInt("savedAppVersion", m_savedAppVersionDef));
//
//            setServerAddress(settings.getString("serverAddress", m_serverAddressDef));
//            setServerPort(settings.getInt("serverPort", m_serverPortDef));
//
//            setReportStatuses(settings.getBoolean("sendStatuses", m_reportStatusesDef));
//
//            String[] statusesLengths = TextUtils.split(settings.getString("statuses_lengths", ""), ",");
//            String strJoinedStatuses = settings.getString("statuses", "");
//            int start = 0;
//            m_statuses.clear();
//            for (String strLen : statusesLengths) {
//                int len = Integer.parseInt(strLen);
//                m_statuses.add(strJoinedStatuses.substring(start, start + len));
//                start += len;
//            }
//            setSelectedStatusIdx(settings.getInt("selectedStatusIdx", m_selectedStatusIdx));
//
//            setLogFilename(settings.getString("logFilename", m_logFilenameDef));
//
//            setUseSchedule(settings.getBoolean("useSchedule", m_useScheduleDef));
//            setScheduleDaysFlag(settings.getInt("scheduleDaysFlag", m_scheduleDaysFlag));
//            setFromHour(settings.getInt("fromHour", m_fromHour));
//            setFromMinute(settings.getInt("fromMinute", m_fromMinute));
//            setToHour(settings.getInt("toHour", m_toHour));
//            setToMinute(settings.getInt("toMinute", m_toMinute));
//
//            setUsePin(settings.getBoolean("usePin", m_usePin));
//            setPin(settings.getString("pin", m_pin));
//
//            setShowServiceIcon(settings.getBoolean("showServiceIcon", m_showServiceIconDef));
//
//            setSendIntervalSec(false, settings.getInt("sendIntervalUnplugged", m_sendIntervalSecDef[0]));
//            setUpdateDistanceM(false, settings.getInt("updateDistanceUnpluggedM", m_updateDistanceMDef[0]));
//            setUpdatePeriodSec(false, settings.getInt("updatePeriodUnpluggedSec", m_updatePeriodSecDef[0]));
//            setFilterPeriodSec(false, settings.getInt("filterPeriodUnpluggedSec", m_filterTimeoutSecDef[0]));
//
//            setSendIntervalSec(true, settings.getInt("sendIntervalPlugged", m_sendIntervalSecDef[1]));
//            setUpdateDistanceM(true, settings.getInt("updateDistancePluggedM", m_updateDistanceMDef[1]));
//            setUpdatePeriodSec(true, settings.getInt("updatePeriodPluggedSec", m_updatePeriodSecDef[1]));
//            setFilterPeriodSec(true, settings.getInt("filterPeriodPluggedSec", m_filterTimeoutSecDef[1]));
//
//            setUseNetworkSource(false, settings.getBoolean("useNetworkSourceUnplugged", m_useNetworkSourceDef[0]));
//            setUseGpsSource(false, settings.getBoolean("useGpsSourceUnplugged", m_useGpsSourceDef[0]));
//            //setUseAccelerometerControl(_settings.getBoolean("useAccelerometerControl", m_useAccelerometerControlDef));
//            setUseNetworkSource(true, settings.getBoolean("useNetworkSourcePlugged", m_useNetworkSourceDef[1]));
//            setUseGpsSource(true, settings.getBoolean("useGpsSourcePlugged", m_useGpsSourceDef[1]));
//            setShakeThreshold(settings.getInt("shakeThreshold", m_shakeThresholdDef));
//            setDisableGpsAfterSec(settings.getInt("disableGpsAfterTimeout", m_disableGpsAfterSecDef));
//        }
//        catch (Exception e) {
//            logger().logError(e);
//        }
//    }

    private void startForgetPinProvidedTimer() {
        m_forgetPinProvidedTimerTask = new Timer();
        m_forgetPinProvidedTimerTask.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    try {
                        setPinProvided(false);
                    } catch (Exception e) {
                        logger().logError(e);
                    } catch (Error er) {
                        logger().logError(er);
                    }
                } catch (Exception e) {
                    logger().logError(e);
                } catch (Error er) {
                    logger().logError(er);
                }
            }
        }, 60000);
    }

    private void stopForgetPinProvidedTimer() {
        if (m_forgetPinProvidedTimerTask != null)
            m_forgetPinProvidedTimerTask.cancel();
    }

    private void restartForgetPinProvidedTimer() {
        stopForgetPinProvidedTimer();
        if (getPinProvided())
            startForgetPinProvidedTimer();
    }

    boolean m_assistanceRequestModified = false;

    public void setAssistanceRequestModified(boolean _val) {
        m_assistanceRequestModified = _val;
    }

    public boolean getAssistanceRequestModified() {
        return m_assistanceRequestModified;
    }


    public void SaveLastPositionAndZoom(
            Context _ctx,
            IGeoPoint _ptCenter,
            int _zoom
    ) {
        if (_ctx != null) {
            SharedPreferences settings = _ctx.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();

            editor.putInt("last_zoom", _zoom);
            editor.putFloat("last_lat", (float) _ptCenter.getLatitude());
            editor.putFloat("last_lon", (float) _ptCenter.getLongitude());

            editor.apply();
        }
    }

    public boolean LoadLastPositionAndScale(
            Context _ctx,
            GeoPoint _ptCenter,
            AtomicInteger _atZoom
    ) {
        SharedPreferences preferences = _ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean lastPositionExists = preferences.contains("last_lat");

        if (lastPositionExists) {
            _atZoom.set(preferences.getInt("last_zoom", 3));
            _ptCenter.setCoordsE6(
                    (int) (1e6 * preferences.getFloat("last_lat", 0)),
                    (int) (1e6 * preferences.getFloat("last_lon", 0))
            );
        }

        return lastPositionExists;
    }

    public static long getMemoryCacheMaxSize() {
        return 2L * 1024 * 1024;
    }

    public static long getMemoryCacheTrimSize() {
        return 1L * 1024 * 1024;
    }

    public boolean generateAssistanceRequests() {
        return false;
    }

    public boolean getUserThrift() {
        return false;
    }

    public String getTruststorePath() {
        return  "okp_new.jks";
    }

    public String getTruststorePassphrase() {
        return  "okp_pass";
    }


    public int getThriftTimeout () { return 5000; }
    public String getThriftHost () { return "dispatch.navitel.su";}
    public int getThriftPort () { return 19991;}

    private long m_lastStatsSavedMS = 0;
    public long getLastStatsSavedMS() { return m_lastStatsSavedMS; }
    public void setLastStatsSavedMS(long _val) {  m_lastStatsSavedMS = _val; }

    private long m_lastProviderDisabledWarningMS = 0;
    public long getLastProviderDisabledWarningMS() { return m_lastProviderDisabledWarningMS; }
    public void setLastProviderDisabledWarningMS(long _val) {  m_lastProviderDisabledWarningMS = _val; }
}
