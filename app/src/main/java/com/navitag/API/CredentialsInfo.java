package com.navitag.API;

/**
 * Created by eugene on 10/3/16.
 */
public class CredentialsInfo {
    public String login;
    public String password;
    public String objectName;

    public CredentialsInfo(String _l, String _p, String _on) {
        login = _l;
        password = _p;
        objectName = _on;
    }

    public boolean isValid () {
        return login != null && password != null && objectName != null &&
                login.length() > 0 && password.length() > 0 && objectName.length() > 0;
    }
}
